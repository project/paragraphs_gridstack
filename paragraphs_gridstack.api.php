<?php

/**
 * @file
 * Hooks and documentation related to Paragraphs Gridstack module.
 */

/**
 * Alter the paragraphs_gridstack providers.
 *
 * @param array $providers
 *   An associative array containing providers.
 */
function hook_paragraphs_gridstack_libraries_providers_alter(array &$providers) {
}
