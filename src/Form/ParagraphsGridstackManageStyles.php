<?php

namespace Drupal\paragraphs_gridstack\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\paragraphs_gridstack\GridstackBreakpointsManagerInterface;
use Drupal\paragraphs_gridstack\GridstackStylesManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ParagraphsGridstackEditForm.
 *
 * Provides the edit form for our ParagraphsGridstack optionset entity.
 */
class ParagraphsGridstackManageStyles extends EntityForm {

  /**
   * An entity query factory for the ParagraphsGridstack entity type.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $entityStorage;

  /**
   * Gridstack Manager for the breakpoints.
   *
   * @var \Drupal\paragraphs_gridstack\GridstackBreakpointsManagerInterface
   */
  protected GridstackBreakpointsManagerInterface $gridstackBreakpointsManager;

  /**
   * The GridstackStylesManager.
   *
   * @var \Drupal\paragraphs_gridstack\GridstackStylesManager
   */
  protected GridstackStylesManager $gridstackStylesManager;

  /**
   * Construct the ParagraphsGridstackFormBase.
   *
   * For simple entity forms, there's no need for a constructor. Our form
   * base, however, requires an entity query factory to be injected into it
   * from the container. We later use this query factory to build an entity
   * query for the exists() method.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   An entity query factory for the ParagraphsGridstack entity type.
   * @param \Drupal\paragraphs_gridstack\GridstackBreakpointsManagerInterface $gridstack_breakpoints_manager
   *   A GridstackBreakpointsManagerInterface service.
   * @param \Drupal\paragraphs_gridstack\GridstackStylesManager $gridstack_manage_styles
   *   A GridstackStylesManager service.
   */
  final public function __construct(EntityStorageInterface $entity_storage, GridstackBreakpointsManagerInterface $gridstack_breakpoints_manager, GridstackStylesManager $gridstack_manage_styles) {
    $this->entityStorage = $entity_storage;
    $this->gridstackBreakpointsManager = $gridstack_breakpoints_manager;
    $this->gridstackStylesManager = $gridstack_manage_styles;
  }

  /**
   * Factory method for ParagraphsGridstackFormBase.
   *
   * When Drupal builds this class it does not call the constructor directly.
   * Instead, it relies on this method to build the new object. Why? The class
   * constructor may take multiple arguments that are unknown to Drupal. The
   * create() method always takes one parameter -- the container. The purpose
   * of the create() method is twofold: It provides a standard way for Drupal
   * to construct the object, meanwhile it provides you a place to get needed
   * constructor parameters from the container.
   *
   * In this case, we ask the container for an entity query factory. We then
   * pass the factory to our class as a constructor parameter.
   */
  public static function create(ContainerInterface $container): ParagraphsGridstackManageStyles {
    $form = new static(
      $container->get('entity_type.manager')->getStorage('paragraphs_gridstack'),
      $container->get('paragraphs_gridstack.breakpoints_manager'),
      $container->get('paragraphs_gridstack.styles_manager'),
    );

    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   *
   * Builds the entity manage styles form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   Associative array containing the ParagraphsGridstack manage
   *   styles form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Build the form.
    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Css classes'),
      '#maxlength' => 255,
      '#size' => 100,
      '#description' => $this->t('Place your css class, if you need multiple styles use: (.d-flex .flex-column)'),
    ];

    $options = $this->gridstackStylesManager->getStylesList($this->getEntity()->id());
    $options_list = [];
    foreach ($options as $option) {
      $options_list[$option['class']] = $option['name'];
    }

    if (empty($options_list)) {
      $form['missing_styles_message'] = [
        '#type' => 'container',
        '#weight' => -100,
        '#attributes' => [
          'class' => ['gridstack-missing-styles-message'],
        ],
      ];
      $form['missing_styles_message']['message_text'] = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => t('Missing style options, please set it for the:'),
        '#attributes' => [
          'class' => ['gridstack-missing-styles-message-link'],
        ],
      ];
      $form['missing_styles_message']['message_link'] = [
        '#title' => 'Gridstack Optionset',
        '#type' => 'link',
        '#url' => Url::fromRoute('entity.paragraphs_gridstack.edit_form', ['paragraphs_gridstack' => $this->getEntity()->id()]),
        '#attributes' => [
          'class' => ['gridstack-missing-styles-message-link'],
        ],
      ];
    }
    else {
      $form['options'] = [
        '#type' => 'select',
        '#title' => $this->t('Choose style options'),
        '#options' => $options_list,
        '#description' => $this->t('What options you choose?'),
      ];
    }

    // Return the form.
    return $form;
  }

}
