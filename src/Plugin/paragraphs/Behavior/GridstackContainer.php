<?php

namespace Drupal\paragraphs_gridstack\Plugin\paragraphs\Behavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Drupal\paragraphs_gridstack\GridstackBreakpointsManagerInterface;
use Drupal\paragraphs_gridstack\ParagraphsGridstackManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Gridstack container behavior implementation.
 *
 * @ParagraphsBehavior(
 *   id = "gridstack_container",
 *   label = @Translation("Gridstack Container"),
 *   description = @Translation("Provides Gridstack layouts."),
 *   weight = 0,
 * )
 */
class GridstackContainer extends ParagraphsBehaviorBase implements ContainerFactoryPluginInterface {

  /**
   * The cache key.
   *
   * @var string
   */
  protected const CACHE_KEY = 'paragraphs_gridstack';

  /**
   * The Gridstack columns options.
   *
   * @var array
   */
  protected static array $columnsOptions = [];

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * ParagraphsGridstackManagerInterface definition.
   *
   * @var \Drupal\paragraphs_gridstack\ParagraphsGridstackManagerInterface
   */
  protected ParagraphsGridstackManagerInterface $gridstackOptionsetsManager;

  /**
   * GridstackBreakpointsManagerInterface definition.
   *
   * @var \Drupal\paragraphs_gridstack\GridstackBreakpointsManagerInterface
   */
  protected GridstackBreakpointsManagerInterface $gridstackBreakpointsManager;

  /**
   * LibraryDiscoveryInterface definition.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected LibraryDiscoveryInterface $libraryDiscovery;

  /**
   * ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * GridstackContainer plugin constructor.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactoryInterface $container_factory,
    ParagraphsGridstackManagerInterface $gridstack_optionsets_manager,
    GridstackBreakpointsManagerInterface $gridstack_breakpoints_manager,
    LibraryDiscoveryInterface $library_discovery,
    ModuleHandlerInterface $module_handler,
    LoggerChannelFactoryInterface $logger_factory,
    CacheBackendInterface $cache_backend
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager);
    $this->configFactory = $container_factory;
    $this->gridstackOptionsetsManager = $gridstack_optionsets_manager;
    $this->gridstackBreakpointsManager = $gridstack_breakpoints_manager;
    $this->libraryDiscovery = $library_discovery;
    $this->moduleHandler = $module_handler;
    $this->logger = $logger_factory->get('paragraph_gridstack');
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('paragraphs_gridstack.manager'),
      $container->get('paragraphs_gridstack.breakpoints_manager'),
      $container->get('library.discovery'),
      $container->get('module_handler'),
      $container->get('logger.factory'),
      $container->get('cache.default'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode): void {
    $libraries = ['paragraphs_gridstack/paragraphs_gridstack.static'];
    $settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'settings', '');
    if (empty($settings)) {
      return;
    }

    $column_options = $this->buildColumnsOptions();
    // Add additional attributes to the paragraph container.
    $build['#attributes']['class'][] = 'paragraphs-gridstack';
    $build['#attributes']['id'] = Html::getUniqueId('paragraphs-gridstack');

    // Dynamic creation of the attached libraries.
    foreach ($settings as $breakpoint_name => $breakpoint_value) {
      foreach ($breakpoint_value as $name => $value) {
        $setting_name = substr($name, strlen($breakpoint_name) + 1);
        $attribute_name = 'data-' . preg_replace('~[\\\\/:*?"<>|._]~', '-', $name);
        switch ($setting_name) {
          case 'columns':
            // Add library with provider, received from buildColumnsOptions().
            $libraries[] = "{$column_options[$value]['provider']}/{$value}";
            $build['#attributes'][$attribute_name] = $column_options[$value]['columns'];
            break;

          case 'storage':
            $build['#attributes'][$attribute_name] = trim(preg_replace('/\s\s+/', '', $value));
            break;
        }
      }
    }

    $libraries = array_filter(array_unique($libraries));
    $attached_libraries = $build['#attached']['library'] ?? [];
    $build['#attached']['library'] = array_merge($libraries, $attached_libraries);

    // Get the default or selected optionset.
    $default_optionset = $paragraph->getBehaviorSetting(
      $this->getPluginId(),
      'optionset',
      'paragraphs_gridstack.optionset.default'
    );

    $settings = &$build['#attached']['drupalSettings']['paragraphs_gridstack'];
    /** @var \Drupal\paragraphs_gridstack\Entity\ParagraphsGridstack $optionset_entity */
    $optionset_entity = $this->gridstackOptionsetsManager->get($default_optionset);
    $settings['optionset'] = $optionset_entity->getRawData();

    // Get the provider of the breakpoints.
    $breakpoints_provider = $optionset_entity->get('breakpoints_provider');
    /** @var \Drupal\breakpoint\Breakpoint[] $breakpoints */
    $breakpoints = $this->gridstackBreakpointsManager->getBreakpointsByCondition($breakpoints_provider);
    $media_queries = $this->gridstackBreakpointsManager->getBreakpointsMediaQuery($breakpoints);

    $settings['responsive']['breakpoints'] = $media_queries;
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): array {
    $optionsets = $this->buildOptionsetsOptions();

    // Get the default or selected optionset.
    $default_optionset = $paragraph->getBehaviorSetting(
      $this->getPluginId(),
      'optionset',
      'paragraphs_gridstack.optionset.default'
    );

    $form['optionset'] = [
      '#type' => 'select',
      '#options' => $optionsets,
      '#default_value' => $default_optionset,
      '#title' => $this->t('Choose the Gridstack Optionset:'),
      '#description' => Link::createFromRoute(
        $this->t('You can manage Gridstack Optionsets here.'),
        'entity.paragraphs_gridstack.list',
      )->toRenderable(),
    ];

    $form['buttons'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'pg-action-parent-container',
        'class' => ['paragraphs-gridstack-action-parent-container'],
      ],
    ];

    $form['buttons']['set_by_template'] = [
      '#type' => 'button',
      '#title' => $this->t('Set by template'),
      '#value' => $this->t('Set by template'),
      '#attributes' => [
        'id' => 'pg-action-set-by-template',
        'class' => ['paragraphs-gridstack-operation'],
        'title' => $this->t('Press to set elements like in template. You can manage template settings in the Gridstack Optionset.'),
      ],
    ];

    $form['buttons']['restore'] = [
      '#type' => 'button',
      '#title' => $this->t('Restore settings'),
      '#value' => $this->t('Restore settings'),
      '#attributes' => [
        'id' => 'pg-action-restore',
        'class' => ['paragraphs-gridstack-operation'],
        'title' => $this->t('Press to restore settings of the latest revision.'),
      ],
    ];

    // @todo Handle AJAX updating.
    /** @var \Drupal\paragraphs_gridstack\Entity\ParagraphsGridstack $optionset_entity */
    $optionset_entity = $this->gridstackOptionsetsManager->get($default_optionset);

    // Get the provider of the breakpoints.
    $breakpoints_provider = $optionset_entity->get('breakpoints_provider');
    /** @var \Drupal\breakpoint\Breakpoint[] $breakpoints */
    $breakpoints = $this->gridstackBreakpointsManager->getBreakpointsByCondition($breakpoints_provider);

    usort($breakpoints, function ($a, $b) {
      return $a->pluginDefinition['weight'] <=> $b->pluginDefinition['weight'];
    });

    // Get the active breakpoint, after sort it should be the last one.
    $active_breakpoint = end($breakpoints)->getPluginId();

    // Wrap screen modes buttons into container.
    $form['buttons']['modes'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'pg-action-container',
        'class' => ['paragraphs-gridstack-actions-container'],
      ],
    ];

    // Dynamic creation of the buttons.
    foreach ($breakpoints as $breakpoint) {
      $label = $breakpoint->pluginDefinition['label'];
      $bp_class = strtolower("{$label}-breakpoint");

      $form['buttons']['modes']["breakpoint_{$label}"] = [
        '#type' => 'button',
        '#title' => $label,
        '#value' => $label,
        '#attributes' => [
          'id' => "pg-action-{$bp_class}",
          'class' => ['paragraphs-gridstack-action', $bp_class],
          'data-breakpoint-class' => $bp_class,
        ],
      ];

      if ($breakpoint->getPluginId() === $active_breakpoint) {
        // Add active class to the button of the breakpoints options.
        $form['buttons']['modes']["breakpoint_{$label}"]['#attributes']['class'][] = 'active';
      }
    }

    // Fetch available gridstack options/libraries data.
    $gridstack_options = $this->buildColumnsOptions();
    // Get list of the available columns.
    $columns_options = array_filter(
      array_combine(
        array_keys($gridstack_options),
        array_column($gridstack_options, 'label'),
      ),
    );

    // Template settings of the optionset.
    $template_default = $optionset_entity->get('template');

    $mapping = [
      'columns' => [
        'type' => 'select',
        'title' => $this->t('Columns:'),
        'options' => $columns_options,
        'default' => NULL,
      ],
      'template' => [
        'type' => 'textarea',
        'title' => $this->t('Template:'),
        'default' => $template_default ?? '{}',
      ],
      'previous' => [
        'type' => 'textarea',
        'title' => $this->t('Previous:'),
        'default' => '{}',
      ],
      'storage' => [
        'type' => 'textarea',
        'title' => $this->t('Storage:'),
        'default' => '{}',
      ],
    ];

    foreach ($breakpoints as $breakpoint) {
      $label = $breakpoint->pluginDefinition['label'];
      $bp_class = strtolower("{$label}-breakpoint");

      $breakpoint_settings[] = 'paragraphs-gridstack-settings';
      if ($breakpoint->getPluginId() == $active_breakpoint) {
        $breakpoint_settings[] = 'active';
      }

      $form['settings'][$breakpoint->getPluginId()] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [...$breakpoint_settings],
          'data-breakpoint-class' => $bp_class,
        ],
      ];

      // Build label of the breakpoint as plugin value.
      $form['settings'][$breakpoint->getPluginId()]["{$breakpoint->getPluginId()}.label"] = [
        '#type' => 'value',
        '#value' => $breakpoint->getLabel(),
        '#access' => FALSE,
      ];

      foreach ($mapping as $setting_key => $settings) {
        // Go through all settings and build form elements.
        $machine_name = "{$breakpoint->getPluginId()}.$setting_key";
        $default_value = $paragraph->getBehaviorSetting(
          $this->getPluginId(),
          ['settings', $breakpoint->getPluginId(), $machine_name],
          $settings['default'],
        );

        // Prepare identifier for element.
        $identifier = preg_replace('/\W/', '_', $machine_name);

        $form['settings'][$breakpoint->getPluginId()][$machine_name] = [
          '#type' => $settings['type'],
          '#title' => $settings['title'],
          '#default_value' => $default_value,
          '#attributes' => [
            'class' => [$bp_class, $identifier],
            'data-setting-type' => $setting_key,
            'data-setting-breakpoint' => $bp_class,
            'data-setting-identifier' => $identifier,
          ],
        ];

        if (!empty($settings['options'])) {
          $form['settings'][$breakpoint->getPluginId()][$machine_name]['#options'] = $settings['options'];
        }
      }
    }

    $form['#attached']['library'][] = 'paragraphs_gridstack/paragraphs_gridstack.base';

    // @todo attach library on ajax
    // @fixme temp solution for the gridstack 8.x
    $form['#attached']['library'][] = 'paragraphs_gridstack/paragraphs_gridstack.columns.12';
    $form['#attached']['library'][] = 'paragraphs_gridstack/paragraphs_gridstack.columns.24';

    $form['#attached']['drupalSettings']['paragraphs_gridstack'] = [
      'libraries_data' => $gridstack_options,
      // @todo that's not active, but first, should be renamed?
      'active_breakpoint' => preg_replace('/\W/', '_', $active_breakpoint),
      'gridstack_optionset' => $optionset_entity->getRawData(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph): array {
    $settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'settings', '');
    $column_options = $this->buildColumnsOptions();

    if (empty($settings)) {
      return [];
    }

    $settings_summary = [];
    foreach ($settings as $breakpoint_name => $breakpoint_value) {
      $settings_summary[] = $this->t('<b>@breakpoint settings:</b> @settings columns', [
        '@breakpoint' => $breakpoint_value["$breakpoint_name.label"],
        '@settings' => $column_options[$breakpoint_value["$breakpoint_name.columns"]]['columns'],
      ]);
    }

    return $settings_summary;
  }

  /**
   * Return array of gridstack options sets.
   *
   * @return array
   *   Array of options.
   */
  public function buildOptionsetsOptions(): array {
    $options = [];
    $optionsets = $this->gridstackOptionsetsManager->listAll();

    foreach ($optionsets as $optionset) {
      $config = $this->configFactory->get($optionset);
      $options[$optionset] = $config->get('label');
    }

    return $options;
  }

  /**
   * Return array of available columns.
   *
   * @return array
   *   Array of options.
   */
  public function buildColumnsOptions(): array {
    if (!self::$columnsOptions) {
      $cached = $this->cacheBackend->get(self::CACHE_KEY);

      if ($cached) {
        return self::$columnsOptions = $cached->data;
      }

      $options = [];
      $libraries = [];

      $libraries_providers = ['paragraphs_gridstack'];
      $this->moduleHandler->alter('gridstack_libraries_providers', $libraries_providers);
      if (empty($libraries_providers)) {
        $this->logger->notice('No libraries providers found for Gridstack.');
      }

      foreach ($libraries_providers as $provider) {
        $librariesList = $this->libraryDiscovery
          ->getLibrariesByExtension($provider);

        // Set the provider attribute for each library.
        foreach ($librariesList as &$item) {
          $item['provider'] = $provider;
        }
        $libraries[] = $librariesList;
      }

      if (empty($libraries)) {
        $this->logger->notice('No libraries found for Gridstack.');
      }

      // Merge all libraries into one array.
      $libraries = array_merge(...$libraries);

      foreach ($libraries as $machine_name => $properties) {
        if (!empty($properties['gridstack'])) {
          $options[$machine_name]['label'] = $properties['label'];
          $options[$machine_name]['columns'] = $properties['columns'];
          // Set the provider value to be able to attach required library.
          $options[$machine_name]['provider'] = $properties['provider'];
        }
      }

      $this->cacheBackend->set(self::CACHE_KEY, $options, CacheBackendInterface::CACHE_PERMANENT, ['columns_options']);
      self::$columnsOptions = $options;
    }

    return self::$columnsOptions;
  }

}
