<?php

namespace Drupal\paragraphs_gridstack;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class GridstackStylesManager.
 *
 * Get styles from existing ParagraphsGridstack entity.
 */
class GridstackStylesManager {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Create ParagraphsGridstackStyleManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Config factory service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactory $config) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config;
  }

  /**
   * Get List of existing styles.
   *
   * @param string $optionset_id
   *   Optionset id.
   *
   * @return array
   *   Array of existing styles.
   */
  public function getStylesList(string $optionset_id) {
    $config_entity = $this->entityTypeManager->getStorage('paragraphs_gridstack')->load($optionset_id);

    $styles_list = [];
    if (empty($config_entity->get('styles_list'))) {
      return $styles_list;
    }
    $styles_list = $this->parseStylesList($config_entity->get('styles_list'));
    return $styles_list;
  }

  /**
   * Get style name.
   *
   * @param string $optionset_id
   *   Optionset id.
   * @param string $style
   *   Style.
   *
   * @return false|mixed|void
   *   If exist style label, if not false.
   */
  public function getStyleName(string $optionset_id, string $style) {
    $styles_list = $this->getStylesList($optionset_id);
    foreach ($styles_list as $item) {
      if ($item['class'] == $style) {
        return $item['name'];
      }
    }

    return FALSE;
  }

  /**
   * Get existing styles and parse to the list.
   *
   * @param string $styles
   *   Existing styles.
   *
   * @return array
   *   Parsed array of existing styles.
   */
  public function parseStylesList(string $styles) {
    $configured_style = [];
    foreach (explode("\n", $styles) as $style) {
      $style = trim($style);
      // Parse.
      [$selector, $label] = explode('|', $style);
      $configured_style[] = [
        'name' => trim($label),
        'class' => trim($selector),
      ];
    }
    return $configured_style;
  }

}
