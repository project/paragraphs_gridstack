**Paragraph Gridstack** - provides a new paragraphs behavior plugin
for dashboard layout and creation. Making a drag-and-drop,
the multi-column responsive dashboard has never been more straightforward.

This module uses the [GridstackJS](https://github.com/gridstack/gridstack.js "Click to view a git repo") library.
Inspired by no-longer maintained gridster, built with love.

Check http://gridstackjs.com and these demos.


Module and dependencies installation:
-------------------------------------


### The composer way #1

1) Run `composer require wikimedia/composer-merge-plugin`
2) Update the root composer.json file. For example:

```yaml
{
  "extra": {
    "merge-plugin": {
      "include": [
        "web/modules/contrib/paragraphs_gridstack/composer.libraries.json"
      ]
    }
  }
}
```

3) Run `composer require drupal/paragraphs_gridstack gridstack/gridstack`,
   and the GridstackJS will automatically be installed into libraries folder.


### The composer way #2

1) Add a custom package to the root composer.json file.

```yaml
{
  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    {
      "type": "package",
      "package": {
        "name": "gridstack/gridstack",
        "version": "8.2.1",
        "type": "drupal-library",
        "dist": {
          "url": "https://registry.npmjs.org/gridstack/-/gridstack-8.2.1.tgz",
          "type": "tar"
        }
      }
    }
  ]
}
```

2) Run `composer require drupal/paragraphs_gridstack gridstack/gridstack`,
   and the GridstackJS will be installed into libraries folder automatically.


### The composer way #3

1) Run `composer require oomphinc/composer-installers-extender`, to install
   an additional package allowing you to install Gridstack.JS as npm-asset from
   asset-packagist.org.

2) Set the new repository for the composer to download npm-asset packages:

```yaml
{
  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    {
      "type": "composer",
      "url": "https://asset-packagist.org"
    }
  ],
}
```

3) Update the extra settings to define new installers types and configure them:

```yaml
{
  "extra": {
    "installer-types": [ "bower-asset", "npm-asset" ],
    "drupal-scaffold": {
      ...
    },
    "installer-paths": {
      "web/core": [
        "type:drupal-core"
      ],
      "web/libraries/{$name}": [
        "type:drupal-library",
        "type:bower-asset",
        "type:npm-asset"
      ],
      ...
    },
  },
}
```

4) Run `composer require drupal/paragraphs_gridstack npm-asset/gridstack:8.2.1`,
   to install module and it's dependencies.

----

🇺🇦 This module is maintained by Ukrainian developers.
Please consider [supporting Ukraine](https://supportukrainenow.org) in a fight for their freedom and safety of Europe.
