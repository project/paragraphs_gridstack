<?php

namespace Drupal\Tests\paragraphs_gridstack\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Tests the Paragraphs Gridstack Behavior Plugin.
 *
 * @group paragraphs_gridstack
 */
class ParagraphsGridstackBehaviorPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'file',
    'system',
    'node',
    'field',
    'field_ui',
    'block',
    'dblog',
    'breakpoint',
    'paragraphs',
    'paragraphs_gridstack',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('paragraph');
    $this->installSchema('system', ['sequences']);
    \Drupal::moduleHandler()->loadInclude('file', 'install');
    \Drupal::moduleHandler()->loadInclude('dblog', 'install');
    \Drupal::moduleHandler()->loadInclude('paragraphs', 'install');
    \Drupal::moduleHandler()->loadInclude('paragraphs_gridstack', 'install');
  }

  /**
   * Tests the behavior settings API.
   */
  public function testBehaviorSettings() {
    // Create a paragraph type.
    $paragraph_type = ParagraphsType::create([
      'label' => 'test_text',
      'id' => 'test_text',
      'behavior_plugins' => [
        'gridstack_container' => [
          'enabled' => TRUE,
        ],
      ],
    ]);
    $paragraph_type->save();

    // Create a paragraph and set its feature settings.
    $paragraph = Paragraph::create([
      'type' => 'test_text',
    ]);
    $feature_settings = [
      'gridstack_container' => [
        'optionset' => 'default',
      ],
    ];
    $paragraph->setAllBehaviorSettings($feature_settings);
    $paragraph->save();
  }

  /**
   * Tests uninstalling a behavior plugin providing module.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testBehaviorUninstall() {
    // Create a paragraph type.
    $paragraph_type = ParagraphsType::create([
      'label' => 'test_text',
      'id' => 'test_text',
      'behavior_plugins' => [
        'gridstack_container' => [
          'enabled' => TRUE,
        ],
      ],
    ]);
    $paragraph_type->save();
    $dependencies = $paragraph_type->getDependencies();
    $plugins = $paragraph_type->getBehaviorPlugins()->getInstanceIds();
    $this->assertSame(['module' => ['paragraphs_gridstack']], $dependencies);
    $this->assertSame(['gridstack_container' => 'gridstack_container'], $plugins);

    // Uninstall plugin providing module.
    $this->container->get('config.manager')->uninstall('module', 'paragraphs_gridstack');

    $paragraph_type = ParagraphsType::load('test_text');
    $this->assertNotNull($paragraph_type);
    $dependencies = $paragraph_type->getDependencies();
    $plugins = $paragraph_type->getBehaviorPlugins()->getInstanceIds();
    $this->assertSame([], $dependencies);
    $this->assertSame([], $plugins);
  }

}
