<?php

namespace Drupal\Tests\paragraphs_gridstack\Functional\ParagraphsGridstackBehaviorPluginAvailableTest;

use Drupal\Tests\paragraphs_gridstack\Functional\ParagraphsGridstackTestBase;

/**
 * Tests availability Paragraphs Gridstack Behavior Plugin.
 */
class ParagraphsGridstackBehaviorPluginAvailableTest extends ParagraphsGridstackTestBase {

  /**
   * Plugin availability test after enabling the module.
   */
  public function testPluginAvailable() {

    // Add a text Paragraph type.
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'text_long');

    // Enable the "Test bold text plugin" to have a behavior form.
    $this->drupalGet('/admin/structure/paragraphs_type/' . $paragraph_type);
    $this->assertSession()->pageTextContains('Gridstack Container');
  }

}
