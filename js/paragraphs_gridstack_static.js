/**
 * @file
 * Provides base JS for gridstack.
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  /**
   * Set up breakpoints listeners for Gridstack.
   *
   * @type {{attach: Drupal.initGridstackBreakpoints.attach}}
   */
  Drupal.behaviors.initGridstackBreakpoints = {
    attach: function (context, settings) {
      // Just define the object if it doesn't exist yet.
      let activeBreakpoints = [];
      // Fetch the breakpoints from the settings.
      let breakpoints = settings.paragraphs_gridstack.responsive.breakpoints;

      /**
       * Defines active breakpoint on window load.
       */
      var handleWindowLoad = function () {
        Object.keys(breakpoints).forEach(function (machine_name) {
          activeBreakpoints[machine_name] = {};

          if (window.matchMedia(breakpoints[machine_name]['media_query']).matches) {
            activeBreakpoints[machine_name] = true;
            $.event.trigger('breakpointActivated', machine_name);
          } else {
            activeBreakpoints[machine_name] = false;
          }
        });
      };

      /**
       * Defines active breakpoint on window resize.
       */
      var handleResize = function () {
        Object.keys(breakpoints).forEach(function (machine_name) {
          if (window.matchMedia(breakpoints[machine_name]['media_query']).matches) {
            // if it wasn't already active, mark it as active
            if (!activeBreakpoints[machine_name]) {
              activeBreakpoints[machine_name] = true;
              $.event.trigger('breakpointActivated', machine_name);
            }
          } else {
            // if it was active, mark it as not active
            if (activeBreakpoints[machine_name]) {
              activeBreakpoints[machine_name] = false;
              $.event.trigger('breakpointDeactivated', machine_name);
            }
          }
        });
      };

      /**
       * Debounce function implementation.
       *
       * @param callback
       *   The callback function to be executed.
       * @param timeout
       *   The timeout in milliseconds.
       *
       * @returns {(function(...[*]): void)|*}
       */
      function debounce(callback, timeout = 300){
        let timer;
        return (...args) => {
          clearTimeout(timer);

          timer = setTimeout(() => {
            callback.apply(this, args);
          }, timeout);
        };
      }

      // Handle the initial load.
      $(window).on('load', handleWindowLoad);

      // Handle the resize event.
      $(window).on('resize', debounce(handleResize, 200));
    }
  };

  /**
   * Set up static gridstack widget.
   *
   * @type {{attach: Drupal.initGridstackStatic.attach}}
   */
  Drupal.behaviors.initGridstackStatic = {
    attach: function (context, settings) {

      /**
       * Catch breakpointActivated event and build Gridstack widget.
       *
       * @param event
       *   The event object.
       * @param breakpoint
       *   The breakpoint machine name.
       */
      const handleBreakpointActivated = function (event, breakpoint) {
        $(once('initGridstackStatic', '.paragraphs-gridstack', context)).each(function () {
          let $paragraph = $(this);
          // @todo maybe better to process names on backend?
          let machine_name = breakpoint.replace(/[._]/g, '-');
          let storage = $paragraph.data(machine_name + '-storage');
          let columns = $paragraph.data(machine_name + '-columns');

          gridstackInit($paragraph, storage, columns);
        });
      };

      $(window).on('breakpointActivated', handleBreakpointActivated);

      /**
       * Init static gridstack widget.
       *
       * @param $paragraph
       *   The paragraph element.
       * @param storage
       *   The serialized data.
       * @param columns
       *   The number of columns.
       */
      function gridstackInit($paragraph, storage, columns) {
        let $container = $paragraph.find('> .field');
        $container.addClass('grid-stack gridstack');
        // Fetch optionset from the settings.
        // @see GridstackContainer::view() method for more details.
        let gridstack_optionset = settings.paragraphs_gridstack.optionset;

        // @todo use options.
        var grid = GridStack.init({
          disableOneColumnMode: true,
          float: gridstack_optionset.float,
          // @todo Use options from the settings.
          //       allow lock
          //       rtl support
          //       no resize/no move per element
        });

        grid.column(columns, 'moveScale');
        // @todo check if needed, maybe this version of gridstack handles this.
        // @todo probably should be updated on resize.
        grid.cellHeight(parseInt($paragraph.width() / columns) + 'px');

        // @todo double check if needed.
        grid.load(storage);
        grid.column(columns, 'moveScale');
        grid.batchUpdate();


        // @todo should be passed from backend.
        var elementsData = $container.find('> .field__item');
        for (var index = 0; index < storage.length; index++) {
          var id = storage[index]['id'];
          var $element;

          $element = id === 0 ?
            $(".grid-stack > .grid-stack-item:not(.grid-stack-item[gs-id]) > .grid-stack-item-content") :
            $(".grid-stack > .grid-stack-item[gs-id=" + id + "] > .grid-stack-item-content");

          $(elementsData[id]).appendTo($element);
        }

        grid.commit();
        grid.setStatic(true);
      }
    }
  };

}(jQuery, Drupal, drupalSettings));
