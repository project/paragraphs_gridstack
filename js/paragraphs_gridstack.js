/**
 * @file
 * Provides base JS for gridstack.
 */
(function($, Drupal, drupalSettings) {
  /**
   * Provides base JS for gridstack.
   *
   * @type {{attach: Drupal.initEditableGridstack.attach}}
   */
  Drupal.behaviors.initEditableGridstack = {
    attach(context, settings) {
      // Attach additional options for a behavior.
      $(once("initEditableGridstack", ".paragraphs-behavior", context)).each(
        function() {
          // Attach additional options for a behavior.
          const $wrapper = $(this);
          // Get textarea and select objects.
          const { active_breakpoint } = drupalSettings.paragraphs_gridstack;

          const $textarea = $(
            `textarea.${active_breakpoint}_storage`,
            $wrapper
          );
          const $select = $(`select.${active_breakpoint}_columns`, $wrapper);

          makeGridstack($wrapper, $textarea, $select, [
            "gridstack_wrapper",
            `gridstack-wrapper-${active_breakpoint}`
          ]);

          // Control switching of display modes.
          $(
            once("gridstackAction", "input.paragraphs-gridstack-action", this)
          ).click(function(event) {
            event.preventDefault();

            const $action = $(this);
            const breakpoint_class = $action.data("breakpoint-class");

            // Update items, which are active.
            $(
              ".paragraphs-gridstack-settings, .paragraphs-gridstack-action"
            ).removeClass("active");
            $(`[data-breakpoint-class=${breakpoint_class}]`).addClass("active");

            // Find related textarea and select.
            const $textarea = $(
              `textarea[data-setting-breakpoint=${breakpoint_class}][data-setting-type="storage"]`
            );
            const $select = $(
              `select[data-setting-breakpoint=${breakpoint_class}]`
            );

            $('div[data-gridstack-meta="true"]', $wrapper).remove();
            makeGridstack($wrapper, $textarea, $select, [
              "gridstack_wrapper",
              `gridstack-wrapper-${active_breakpoint}`
            ]);
          });
        }
      );
    }
  };

  /**
   * Functionality for actions.
   *
   * @todo disable buttons in case of no previous or template data.
   *
   * @type {{attach: Drupal.initEditableGridstackActions.attach}}
   */
  Drupal.behaviors.initEditableGridstackActions = {
    attach(context, settings) {
      // Attach additional options for a behavior.
      $(
        once("initEditableGridstackActions", ".paragraphs-behavior", context)
      ).each(function() {
        $(".paragraphs-gridstack-settings").each(index => {
          const storageField = $(this).find('textarea[class*="storage"]')[
            index
          ];
          const previousField = $(this).find('textarea[class*="previous"]')[
            index
          ];

          if (storageField.textContent.length) {
            previousField.textContent = storageField.textContent;
          }
        });

        // Attach additional options for a behavior.
        $(once("gridstackActionRestore", "#pg-action-restore", this)).click(
          function(event) {
            event.preventDefault();
            $(this).addClass("active");
            const storageField = $(context).find(
              ".paragraphs-gridstack-settings.active " +
                'textarea[class*="storage"]'
            )[0];
            const previousField = $(context).find(
              ".paragraphs-gridstack-settings.active " +
                'textarea[class*="previous"]'
            )[0];

            if (storageField !== undefined && previousField !== undefined) {
              storageField.value = previousField.value;
            }
          }
        );

        // @todo complete this feature.
        $(once("gridstackActionSet", "#pg-action-set-by-template", this)).click(
          function(event) {
            event.preventDefault();
            $(this).addClass("active");
            const storageField = $(context).find(
              ".paragraphs-gridstack-settings.active " +
                'textarea[class*="storage"]'
            )[0];
            const templateField = $(context).find(
              ".paragraphs-gridstack-settings.active " +
                'textarea[class*="template"]'
            )[0];

            if (storageField !== undefined && templateField !== undefined) {
              storageField.value = templateField.value;
            }
          }
        );
      });
    }
  };

  /**
   * Prepare gridstack widget.
   *
   * @todo Complete this function.
   *
   * @param $wrapper
   *   Wrapper object.
   * @param $textarea
   *   Textarea object.
   * @param $select
   *   Select object.
   * @param wrapper_classes
   *   Wrapper classes.
   */
  function makeGridstack($wrapper, $textarea, $select, wrapper_classes) {
    $wrapper.append('<div data-gridstack-meta="true"><div></div></div>');
    const $div_wrapper = $('div[data-gridstack-meta="true"]', $wrapper);
    $div_wrapper.addClass(["gridstack_wrapper", ...wrapper_classes].join(" "));

    const $div = $("div", $div_wrapper);
    const drupal_selector = $textarea.data("drupal-selector");

    // Class grid-stack is rudiment from the previous version of the library.
    $div.addClass(`grid-stack gridstack gridstack-${drupal_selector}`);

    let items = [];

    try {
      if ($textarea.val() !== undefined) {
        items = JSON.parse($textarea.val());
      }
    } catch (exception) {
      console.error(
        "JSON parsing error were found in gridstack data.",
        exception
      );
    }

    const { gridstack_optionset } = drupalSettings.paragraphs_gridstack;

    const $grid = GridStack.init(
      {
        disableOneColumnMode: true,
        float: gridstack_optionset.float
        // @todo Use options from the settings.
        //       allow lock
        //       rtl support
        //       no resize/no move per element
      },
      `.gridstack-${drupal_selector}`
    );

    // @todo probably this is not needed in this function.
    // According to docs of the library:
    //  - this method needed to enable drag and drop and resize,
    //  - this method is shortcut for enableMove(true) and enableResize(true).
    $grid.enableMove(true);
    $grid.enableResize(true);

    $grid.column(checkColumns($select), "moveScale");

    $select.change(() => {
      columnsChanged($select);
    });

    const $parent = $wrapper.parent();
    const $container = $parent.find(".paragraphs-nested tbody");

    // @todo get items from the backend.
    $("tr", $container).each(function(index) {
      let node = items[index] ?? {};

      if (!items[index]) {
        node = {
          id: index,
          w: 10,
          h: 10
        };
      }

      $grid.addWidget(node);
    });

    // Run batch updates, commit changes and update DOM.
    $grid.batchUpdate().commit();

    columnsChanged($select);

    $grid.on("added removed change", function(e, items) {
      writeGrid($grid, $textarea);
    });

    /**
     * Check columns count.
     *
     * @param $select
     *   Select object.
     *
     * @return {number}
     */
    function checkColumns($select) {
      const column_id = $select.val();
      const { libraries_data } = drupalSettings.paragraphs_gridstack;

      if (!libraries_data) {
        // Display error message about missing library.
        console.error("Missing defined gridstack libraries.");
      }

      return libraries_data[column_id].columns;
    }

    /**
     * Change columns count.
     *
     * @param $select
     *   Select object.
     */
    function columnsChanged($select) {
      const columns = checkColumns($select);
      const $container = $div_wrapper.width() > 0 ? $div_wrapper : $wrapper;
      const height = $container.width() / columns;

      writeGrid($grid, $textarea);

      $grid.column(columns, "moveScale");
      $grid.cellHeight(Math.floor(height));
      $grid.load(JSON.parse($textarea.val()));
    }

    /**
     * Write grid data to textarea.
     *
     * @param $grid
     *   Gridstack object.
     * @param $textarea
     *   Textarea object.
     */
    function writeGrid($grid, $textarea) {
      const data = [];
      $grid.engine.nodes.forEach(function(node) {
        // @todo use paragraph id.
        data.push({
          x: node.x,
          y: node.y,
          w: node.w,
          h: node.h,
          id: node.id
        });
      });

      data.sort(function(a, b) {
        return a.id - b.id;
      });

      $textarea.val(JSON.stringify(data, null, "  "));
    }
  }
})(jQuery, Drupal, drupalSettings);
